#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char* argv[]) {
	mkfifo("/tmp/loggerfifo", 0777); //Creación de la tubería FIFO
	int fd = open("/tmp/loggerfifo", O_RDONLY); //Apertura de la FIFO (modo lectura)

	//while(1)
	int salir = 0;
	int aux;
	while (salir==0) 
	{
		char buff[200];
		aux = read(fd, buff, sizeof(buff));
		printf("%s\n", buff);
		if(buff[0]=='~')
		{
			printf("~~~~Juego cerrado. Cerrando logger...~~~~\n");
			salir = 1; 
		}
	}
	close(fd); //Destrucción de la FIFO
	unlink("/tmp/loggerfifo");
	return 0;
};

